# Docker Sandbox


## The hard way

We'll start this exercise by running a command to bring up a database.

There are a few things to consider before we write our command:
1. Engine: what database engine do we like (answer: postres)
2. Port: what port will we access the database with?  (answer: 5432)
3. Persistence: do we want the data to persist beyond the container or is this truly temporary data?  (answer: yes, let's keep the data)

We picked our engine so let's go choose our exact image, off to [the official image on dockerhub](https://hub.docker.com/_/postgres) to find the currently supported tags. For this guide I'll pick the `11-alpine` tag for no particular reason. With that in mind I can pull the image:

```sh
docker pull postgres:11-alpine
```

Now as we assemble the command we can account for the port by using `-p 5432:5432`. To deal with data persistence we will need a so-called volume which allocates some persistent file storage to the container. The dockerhub docs for the image will tell us what we need to know. It's kind of a deep cut in there, but here it is `-v pgdata:/var/lib/postgresql/data`. This will create a directory called `pgdata` nearby which will contain the database files. The docs go on to say that you have to provide an environment variable for the database password: `-e POSTGRES_PASSWORD=mysecretpassword`.

So the run command we end up with is something like:

```sh
docker run -it --rm -v pgdata:/var/lib/postgresql/data -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword postgres:11-alpine
```

The `-it` starts the container detached, and the `--rm` instructs docker to remove the remnants of the container when it exits.

And right about now the "database system is ready to accept connections"!

Generally you would use `-d` instead of `-it` so that the container would run in the background. For now you can hit "ctrl+c" to leave the container.

## Custom work

It is pretty limiting to only be working with stock images, so let's make an image of our own. Follow along with the [Dockerfile](Dockerfile) where we set up an environment for a django app and come back here.

The [full documentation](https://docs.docker.com/engine/reference/builder/) exposes a lot more functionality!

Also take a moment to look at [entrypoint.sh](entrypoint.sh) to see what will execute whenever the container is exectuted.

We can build this image from within the same directory of the Dockerfile

```sh
docker build -t app .
```

And we could run the container from the command line as before, but there are some embellishments that are more easily handled by an orchestrator.

## Slightly easier way

Invoking docker commands on the command line gets pretty cumbersome pretty fast, especially if you have more than one service to deal with. Let's use `docker-compose` to keep track of the specifics that each container needs to run and interact with the rest of the system.

Follow along in [docker-compose.yml](docker-compose.yml) to see how this works.

There is a lot more that is possible with a docker-compose file. I recommend a hearty jaunt through the [documentation](https://docs.docker.com/compose/).

With this file in place you can run any number of `docker-compose` commands to start your instance. The easiest example is:

```sh
docker-compose -p app-demo up -d
```

There are a lot of different commands available through `docker-compose` which will be really useful. Even with this added abstraction there are still a lot of commands to remember, so perhaps we can abstract one level further and really start making life easier.

## Sophisticated way

![Tuxedo Winnie the Pooh](img/tuxedo-winnie-the-pooh.jpg)

Codebases go through changes over time, and this means that eventually the particular instructions for initializing or starting your app _will_ change. To make these inevitable changes as easy to manage as possible it is useful to wrap individual commands into broader actions whose utility is unlikely to change over the lifetime of a project.

For instance, starting the app is something that every developer will have to do on a routine basis. Rather than using this:

```sh
docker-compose -p app-demo up -d
```

which will surely change over the lifetime of the project, we could use:

```sh
make run
```

The need to `run` our app will never go away, but the specifics of how best to start the app running will change annoyingly often.

The program `make` may need to be installed depending on which OS/shell you're using, but once installed you'll only need a `makefile` to define the so-called _targets_ like `run`.  Join us in the [`makefile`](makefile) for a closer look.

You'll often find yourself repeating certain commands over and over; any time you're doing this consider making a target in the `makefile` to encapsulate the action you're trying to perform. This is useful for two reasons: first you won't have to commit that command to memory and type it every day, second any new developer joining the team can learn the patterns for interacting with the app by reading the `makefile`.
