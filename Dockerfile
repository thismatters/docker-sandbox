# Choose a base image. Python is suitable for this django app
FROM python:3.8-alpine

# Install OS dependencies
RUN apk add --no-cache \
    build-base \
    libpq \
    postgresql-dev

# Install python dependencies
RUN python -m pip install \
    Django==3.0.8 \
    psycopg2==2.8.5 \
    dj-database-url==0.5.0

# Install the app
COPY src /app/src/

# Get the runtime instructions
COPY entrypoint.sh /app
RUN chmod +x /app/entrypoint.sh

# Set the runtime user
RUN adduser -D worker -u 1000
USER worker

WORKDIR /app/src

# Set this thing a-running
EXPOSE 8000
ENTRYPOINT ["sh", "/app/entrypoint.sh"]
# The entrypoint makes the container behave as an excecutable
#   this is contrasted with the CMD directive (or the `command` portion
#   of the cli invokation) which, when used, the command will be appended
#   to the end of the entrypoint.

# The [full documentation](https://docs.docker.com/engine/reference/builder/) exposes a lot more functionality!