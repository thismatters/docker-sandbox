# The makefile has a relatively yaml-like simple syntax.
#  The targets are defined at the top level demarked by a colon.
#  The commands to run for the given target are indented by a tab.
#  And seriously here, this must be a tab character. This isn't python
#  get out of here with that four-spaces BS.
#  The monkeytail (@) suppresses the command from being written to the
#  shell. The output of the command will always print to the shell.

# Let's start with a target for `run`
run:
	@docker-compose -p app-demo up -d
# It is good practice to make any reciprocal actions as well; in this case
stop:
	@docker-compose -p app-demo down

# There are some special steps needed when dealing with fresh app
#  environment, e.g. database initialization (which we saw when we
#  ran the postgres container). Let's accumulate these steps in a target
#  called `init`.
init:
	# just start the database and let it run for a while
	@docker-compose -p app-demo up -d db
	@echo "Please stand by..."
	@sleep 30
	# bring up the rest of the app
	@docker-compose -p app-demo up -d
	# create a superuser for the django app
	@docker-compose -p app-demo exec app python manage.py createsuperuser
	@echo "App is now ready and running. Use 'make stop' when you're done."

build:
	@docker-compose -p app-demo build
