# This should always happen everywhere
python manage.py migrate

# However I want the app itself to start differently in dev than production
if [[ $APP_ENV == "dev" ]]; then
    python manage.py runserver 0.0.0.0:8000
elif [[ $APP_ENV == "prod" ]]; then
    # to something else!
    echo "TBD";
fi
